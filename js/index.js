(function(){
    document.addEventListener('DOMContentLoaded', init, false);
    function init(){
        var contents = getContents();
        showContents(contents);
    }

    function clashingColor(color){
        var R = parseInt(color.substring(0,2),16);
        var G = parseInt(color.substring(2,4),16);
        var B = parseInt(color.substring(4,6),16);
        var max = R > G ? (B > R ? B : R) : (B > G ? B : G);
        var min = R < G ? (B < R ? B : R) : (B < G ? B : G);
        var sum = max + min;
        return (sum - R).toString(16) + (sum - G).toString(16) + (sum - B).toString(16);
    }

    function getContents(){
        var contents = document.getElementsByClassName('content');
        for(var i = 0,len = contents.length; i < len; i++){
            var content = contents[i];
            var color = Math.floor(Math.random()*16777215).toString(16);
            var clashing = clashingColor(color);
            content.dataset.view = 'true';
            content.style.marginLeft = i%2 == 0 ? '100%' : '-100%'; 
            content.style.backgroundColor = '#' + color;
            content.style.color = '#' + clashing;
        }
        return contents;
    }

    function showContents(contents){
        for(var i = 0,len = contents.length; i < len; i++){
            var content = contents[i];
            var num = parseInt(content.style.marginLeft);
            num += num > 0 ? -1 : 1;
            content.style.marginLeft = num + '%';
        }
        if(num != 0){
            setTimeout(function(){
                showContents(contents);
            },5);
        }
    }

})();
